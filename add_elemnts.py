# Это просто служебный код который нужен был для базовой настрройки всего
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import declarative_base, sessionmaker

# Создание объекта Base
Base = declarative_base()

# Определение модели для таблицы Клиенты
class Client(Base):
    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)

# Создание соединения с базой данных (замените 'sqlite:///example.db' на ваше соединение)
engine = create_engine('sqlite:///test.db')

# Создание сессии для взаимодействия с базой данных
Session = sessionmaker(bind=engine)
session = Session()

# Создание нового клиента
new_client = Client(first_name='Влад', last_name='Белка')

# Добавление клиента в базу данных
session.add(new_client)

# Создание нового клиента
new_client = Client(first_name='Иван', last_name='Иванов')

# Добавление клиента в базу данных
session.add(new_client)

# Фиксация изменений
session.commit()

# Закрытие сессии
session.close()
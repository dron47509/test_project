import imaplib
import email
from email.header import decode_header
import re
import configparser
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.orm import declarative_base, sessionmaker, relationship

# Создание объекта Base
Base = declarative_base()

# Определение модели для таблицы Полученные данные
class ReceivedData(Base):
    __tablename__ = 'received_data'

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String)
    message = Column(String)


# Определение модели для таблицы Полученные сообщения
class ReceivedMessage(Base):
    __tablename__ = 'received_messages'

    id = Column(Integer, primary_key=True)
    message_id = Column(String)
    
# Определение модели для таблицы Клиенты
class Client(Base):
    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)

# Считывание файла конфигурации
def read_config(file_path='config.ini'):
    config = configparser.ConfigParser()
    with open('config.ini', encoding='utf-8') as config_file:
        config.read_file(config_file)
    return config


def extract_text_between_symbols(input_string, start_symbol, end_symbol):
    # Создание регулярного выражения для поиска текста между символами
    pattern = re.compile(f'{re.escape(start_symbol)}(.*?){re.escape(end_symbol)}')
    
    # Поиск соответствия в строке
    match = pattern.search(input_string)
    
    # Если найдено соответствие, вернуть найденный текст
    if match:
        return match.group(1)
    else:
        return None


def is_valid_email(email):
    # Простая проверка электронного адреса с использованием регулярного выражения
    email_regex = re.compile(r"[^@]+@[^@]+\.[^@]+")
    return bool(re.match(email_regex, email))


def email_address_verification(input_string):
    while True:
        # Разделение строки по запятой и создание списка
        email_list = input_string.split(',')

        # Проверка каждого элемента списка
        check_successful = True
        for email in email_list:
            email = email.strip()  # Удаление лишних пробелов
            if is_valid_email(email) == False:
                check_successful = False
        if check_successful:
            return email_list
    

def connection_to_email():
    config = read_config()
    email_address = config.get('Mail', 'email_address')
    password = config.get('Mail', 'password')
    mail_server = config.get('Mail', 'mail_server')

    # Подключение к серверу почты
    mail = imaplib.IMAP4_SSL(mail_server)

    # Вход в почтовый ящик
    mail.login(email_address, password)

    # Выбор папки (в данном случае, "INBOX")
    mail.select("inbox")

    return mail

def read_emails(mail, trigger_emails, recived_messages, message_template, data_base):
    # Поиск всех сообщений в папке "INBOX"
    status, messages = mail.search(None, "ALL")
    message_ids = messages[0].split()

    for msg_id in message_ids:
        # Получение сообщения по ID
        status, msg_data = mail.fetch(msg_id, "(RFC822)")
        raw_email = msg_data[0][1]

        # Парсинг сообщения
        msg = email.message_from_bytes(raw_email)

        # Получение заголовка "Subject" и декодирование
        subject, encoding = decode_header(msg["Subject"])[0]
        if isinstance(subject, bytes):
            subject = subject.decode(encoding or "utf-8")

        
        message_id = msg.get("Message-ID", "N/A")
        sending_email = extract_text_between_symbols(msg.get('From'), '<', '>')
        message_text = ""
        for part in msg.walk():
            if part.get_content_type() == "text/plain":
                message_text += part.get_payload(decode=True).decode("utf-8")
        message_regex = re.compile(message_template)


        if sending_email in trigger_emails and not(message_id in recived_messages) and bool(re.match(message_regex, message_text)):
            # Регулярные выражения
            name_pattern = re.compile(r'Имя:\s*([^,]+)')
            surname_pattern = re.compile(r'Фамилия:\s*([^,]+)')
            address_pattern = re.compile(r'Адрес:\s*([^,]+)')
            message_pattern = re.compile(r'Сообщение:\s*([^,]+)')

            # Поиск совпадений
            name_match = name_pattern.search(message_text)
            surname_match = surname_pattern.search(message_text)
            address_match = address_pattern.search(message_text)
            message_match = message_pattern.search(message_text)


            name = name_match.group(1).strip()
            surname = surname_match.group(1).strip()
            address = address_match.group(1).strip()
            message = message_match.group(1).strip()       
            
            if name in data_base["Имя"] and surname in data_base["Фамилия"]:
                return[name, surname, address, message, sending_email, message_id]



def main():
    # подключение базы данных и файйла конфигурации
    config = read_config()
    engine = create_engine(config.get('Database', 'database_address'))
    

    # Создание сессии для взаимодействия с базой данных
    Session = sessionmaker(bind=engine)
    session = Session()

    # Зашрузка из файла конфигурации и базы данных
    trigger_emails = email_address_verification(config.get('Triggers', 'trigger_emails'))
    message_template = config.get('Tamplates', 'message_template')
    message_ids = session.query(ReceivedMessage.message_id).all()
    recived_messages = [message_id[0] for message_id in message_ids]
    clients_data = session.query(Client.id, Client.first_name, Client.last_name).all()
    data_base = {client.id: {'Имя': client.first_name, 'Фамилия': client.last_name} for client in clients_data}

    # Закрытие сессии
    session.close()
    mail = connection_to_email()
    while True:
        correct_email = read_emails(mail, trigger_emails, recived_messages, message_template, data_base)
        if correct_email != None:
            print(correct_email)
            # Создание сессии для взаимодействия с базой данных
            Session = sessionmaker(bind=engine)
            session = Session()

            # Создание новых записей
            new_recived_message = ReceivedMessage(message_id=correct_email[5])
            new_data = ReceivedData(first_name=correct_email[0], last_name=correct_email[1], email=correct_email[4], message=correct_email[3])
            recived_messages.append(correct_email[5])


            # Добавление клиента в базу данных
            session.add(new_recived_message)
            session.add(new_data)

            # Фиксация изменений
            session.commit()

            # Закрытие сессии
            session.close()
        else:
            print("Сообщений не поступало")


if __name__ == "__main__":
    main()
    print("Start")
